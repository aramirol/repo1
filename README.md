This section shows the naming convention for projects:

* $PROJECT\_1
  * $REPO\_1
  * $REPO\_2
* $PROJECT\_2
  * $REPO\_3
  * $REPO\_4

---

* $GROUP_1
  * SUBGROUP_1
    * PROJECT_1
    * PROJECT_2
  * SUBGROUP_2
    * PROJECT_3
    * PROJECT_4
* $GROUP_2
  * PROJECT_5

---

In general projects should have human readable names and can contain upper and lowercase names. Repository names have to be all lowercase and only allow numbers, letters, "-" and "\_". For the various branches within a repository the same naming convention has to be followed (all lowercase, letters and numbers are allowed as well as "-" and "\_"). Also usage of emojis within branch names is not allowed.